const express = require("express");
const router = express.Router();
const TaskController = require('../controllers/taskController')


router.post("/", (req, res) => {
	TaskController.createTask(req.body.name).then(result => res.send(result));
})




//1. Create a route for getting a specific task.

router.get("/:id", (req,res) =>{

	TaskController.getTask(req.params.id).then(result => res.send(result));

}) 

module.exports = router;

//5. Create a route for changing the status of a task to "complete".


router.put("/:id/complete", (req,res) => {
	TaskController.completeTask(req.params.id).then(result => res.send(result));
})



