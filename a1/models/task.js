//Models and Schema

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

module.exports = mongoose.model("Task", taskSchema)
//"Task" is the collection name
//Model is sent to controllers
//Model is the structure