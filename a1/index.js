const express = require('express')
const mongoose = require("mongoose")

const taskRoute = require('./routes/taskRoute')

//Server Setup

const app = express();
const port = 3010;
app.use(express.json());
app.use(express.urlencoded({ extended:true}))


//Database connection
mongoose.connect("mongodb+srv://jdmendame:jestandalemendame@jdmendame.7de3z.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
      {

           useNewUrlParser: true,
           useUnifiedTopology: true
      }
)


let db = mongoose.connection

//connection error message
db.on("error", console.error.bind(console, "connection error"))
//connection is successful message
db.once("open",()=> console.log("We're connected to the cloud database."))


//Routes

app.use("/tasks", taskRoute);
//http://localhost:3001/tasks/


app.listen(port, ()=> console.log(`Server running at port:${port}`));

