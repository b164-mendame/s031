const Task = require("../models/task");

module.exports.createTask = (name) => {

	//Create object
	let newTask = new Task({
		name: name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})

}





//2. Create a controller function for retrieving a specific task.

module.exports.getTask = (taskId) => {
return Task.findById(taskId).then( result =>{
	return result;
})

}


//6. Create a controller function for changing the status of a task to "complete".




module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		}

		result.status = "complete";

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}else {
				return updatedTask;
			}
		})


	})
}
